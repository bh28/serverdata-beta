// Evol scripts.
// Author:
//    Vasily_Makarov
// Description:
//    Sleeping and snoring NPC.

000-0-0.gat,29,28,0,1	script	Q'Onan	302,{

    mesn;

    set @q, rand(2);
    if (@q == 0) goto l_Zzz;
    goto l_Snoring;

l_Zzz:
    setcamnpc;
    mesq l("Zzzzzzzzzz");
    close;

l_Snoring:
    setcamnpc;
    mesq l("Rrrr... Pchhhh...");
    close;

}

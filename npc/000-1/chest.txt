// Evol scripts.
// Authors:
//    4144
//    Hal9000
//    Reid
// Description:
//    A treasure chest, with... Treasures inside!
// Variable:
//    x   ShipQuests_TreasureChest
// Values:
//    0   Treasure Chest has not been opened yet.
//    1   Treasure Chest has been opened.

000-1.gat,83,70,0	script	#chest	316;2,{

    set @q, getq(ShipQuests_TreasureChest);
    if (getnpcdir ("") == 4) goto l_Give;

    setnpcdir 2;
    initnpctimer;
    startnpctimer;
    close;

l_Give:
    if (@q > 0) goto l_Close;

    set @item, 514;
    set @count, 1;
    callfunc "InventoryPlace", @item, @count;

    setq ShipQuests_TreasureChest, 1;

    set zeny, zeny + 100;
    getitem "OldBook", 1;
    message strcharinfo(0), l("You open the treasure chest.");

l_Close:
    setnpcdir 6;
    initnpctimer;
    startnpctimer;
    close;

OnTimer160:
    stopnpctimer;
    if (getnpcdir ("") == 2) setnpcdir 4;
    if (getnpcdir ("") == 6) setnpcdir 0;
    end;

}

// Evol scripts.
// Authors:
//    Hal9000
//    Reid
// Description:
//    Couwan is trying to scam the player.
// Variable:
//    x   ShipQuests_Couwan
// Values:
//    0   Never talked with Couwan.
//    1   Spoke, and received the quest scam.
//    2   Done quest scam.

000-1.gat,89,36,0,1	script	Couwan	111;2,{

    set @q, getq(ShipQuests_Couwan);
    if (@q == 1) goto l_QuestGiven;
    if (@q > 1) goto l_QuestDone;

    mesn;
    mesq l("Hello yeye.");
    next;
    mesq l("Look how splendid this landscape is!");
    next;

    menu
        l("Very nice, indeed!"), -,
        l("I don't see anything else than... water?"), l_Water;

    mes "";
    mesn;
    mesq l("Yeye got good eyes and seem to have fully recovered from your injuries...");
    next;
    mesq l("Hey, could yeye please take my box of fish to Gugli?");
    next;

    menu
        l("Sure, but what will I get in exchange?"), -,
        l("I'm sorry, I don't have time right now."), l_Decline;

    mes "";
    mesn;
    mesq l("Yeye ask too much but do too less. Take this box and stop talking.");
    next;

    set @item, 713;
    set @count, 1;
    callfunc "InventoryPlace", @item, @count;

    mesn "Narrator";
    mes col(l("Couwan hands you a box full of fish."), 9);
    getitem "FishBox", 1;
    next;
    mes col(l("The sailor turns his back to you."), 9);

    setq ShipQuests_Couwan, 1;
    close;

l_Water:
    mes "";
    mesn;
    mesq l("Yeye's brain is probably still full of sea water if yeye can't see the beauty of this place.");
    next;
    mesq l("Stupid yeye...");

    close;

l_Decline:
    mes "";
    mesn;
    mesq l("Stupid yeye...");

    close;

l_QuestGiven:
    mesn;
    mesq l("What are you looking at?");
    next;
    mesq l("Yeye still have my box? Stare less and work more. Go give it to Gugli!");

    close;

l_QuestDone:
    mesn;
    mesq l("What are you looking at?");
    next;
    mesq l("Yeye gave my box to Gugli? Nice, nice yeye!");

    close;

}
